from urllib import request

import time

from cv2 import cv2

import numpy as np
from selenium import webdriver



class MyBrower:
    brower = None
    def __init__(self):
        MyBrower.brower = self
        self.driver = webdriver.Firefox(executable_path="F:\TrafficSign\Crawler\geckodriver.exe")


    def get_all_image_url(self,url):
        self.driver.get(url)
        list_Url = []
        i = 1
        while True:
            try:
                herf = self.driver.find_element_by_xpath(
                    '/html/body/div[6]/div[3]/div[3]/div[2]/div/div[2]/div[2]/div/div/div/div/div[1]/div[2]/div[1]/div[' + str(
                        i) + ']/a').get_attribute('href')
                list_Url.append(herf)
                i += 1
            except:
                break
        return list_Url

    def crawl_image(self, url):
        try:
            self.driver.get(url)
        except:
            return None
        src = None
        i = 0
        while src is None:
            i += 1
            try:
                src = self.driver.find_element_by_xpath(
                    '/html/body/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]/a/img').get_attribute('src')
            except:
                print('Error ..........!')
            if i > 10: return None
            print('faild : \t ' + str(i))
            time.sleep(0.3)
        image = self.url_to_image(src)

        return image

    def url_to_image(self, url):
        image = None
        try:
            resp = request.urlopen(url)
            image = np.asarray(bytearray(resp.read()), dtype="uint8")
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        except:
            print("URL error:" + url)
        # return the image
        return image

