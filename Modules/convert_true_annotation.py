import glob
import os

import xmltodict

xml_files = glob.glob('C:\\Users\\Nguyen Anh Binh\\Downloads\\Crawler\Xml\\*.xml')

lines = []


def extract_value(objec):
    name = objec['name']
    box = objec['bndbox']
    xmin, ymin, xmax, ymax = box['xmin'], box['ymin'], box['xmax'], box['ymax']
    return '{} {} {} {} {}'.format(name, xmin, ymin, xmax, ymax)


for xml_file in xml_files:
    data = xmltodict.parse(open(xml_file).read())['annotation']

    frame_index = os.path.split(xml_file)[-1].split('_')[0]
    objs = data['object']
    if objs.__class__ == list:
        for obj in objs:
            sub_line = extract_value(obj)
            line = '{} {}'.format(frame_index, sub_line)
            lines.append(line)
    else:
        sub_line = extract_value(objs)
        line = '{} {}'.format(frame_index, sub_line)
        lines.append(line)

out_file = open(r'D:\Github\SSD_Traffic_Detector\Files\new_answer.txt', 'w')
lines = sorted(lines, key=lambda x: int(x.split(' ')[0]))
for line in lines:
    out_file.write(line + '\n')
