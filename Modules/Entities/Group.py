import operator


class Group:
    def __init__(self):
        self.objects = []
        self.label = None
        self.score = None

    def getAvgScore(self, style=1):
        size = len(self.objects)
        labels = {}  # label format {labelid, điểm của label đó}
        for object in self.objects:
            key = object.label['name']
            if key not in labels:
                labels[key] = 0
            if style == 1:
                labels[key] += object.score
            if style == 2:
                labels[key] += 1  # testing

        for key in labels:
            labels[key] = labels[key] / size

        sorted_labels = sorted(labels.items(), key=operator.itemgetter(1))
        highest_label_name = max(labels.items(), key=operator.itemgetter(1))[0]
        highest_label_score = labels[highest_label_name]
        self.label = highest_label_name
        self.score = highest_label_score
        return sorted_labels

    def balance_object(self, style=1):
        self.getAvgScore(style)
        for object in self.objects:
            object.label['name'] = self.label
            object.score = self.score

    def check_object_belong_to_Group(self, c_object):
        for object in self.objects:
            if object.isInside(c_object.GetCentroid()):
                return True
        return False

    def add_object(self, object):
        self.objects.append(object)
