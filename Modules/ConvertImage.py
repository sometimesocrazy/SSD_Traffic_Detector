import os

import cv2

sourcepath = 'G:\Camera\GTSRB\GTSRB\Final_Test\Images'
despath = 'G:\Camera\GTSRB\GTSRB_Jpg\Final_Test'

def load_images_from_folder(folder):
    images = {}
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))
        if img is not None:
            images[filename.split('.')[0]] = img
    return images


images = load_images_from_folder(sourcepath)

lines = []
for filename in images.keys():
    image = images[filename]
    path = '{}/{}.jpg'.format(despath, filename)
    cv2.imwrite(path,images[filename])
    print(filename)
