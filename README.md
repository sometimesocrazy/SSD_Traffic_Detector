# SSD Traffic Detector

This is an implementation of [SSD](https://arxiv.org/abs/1512.02325v5) on Python 3, TensorFlow and TensorFlow Object Detection. The model generates bounding boxes for each instance of an object in the image.

# Installation

First install [TensorFlow](https://www.tensorflow.org/install/) 
    
``` bash
# For CPU
pip install tensorflow
# For GPU
pip install tensorflow-gpu
``` 

Second [Object detection TensorFlow](https://github.com/tensorflow/models/tree/master/research/object_detection) - Creating accurate machine learning models capable of localizing and identifying multiple objects in a single image remains a core challenge in computer vision

Read [Object detection's Installation](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md) for easier install

# Quick start

### Download Model Graph

[Model graph](https://drive.google.com/open?id=1-adrPf1ozRmGsax9hiNRoOXdSwaLNtSR) and extract to directory (SSD_Traffic_Detector/graph/...)

### Run command line example

```commandline
python3 Examples\Demo.py -m graph\ssd_mobilenet_v1_coco_11_06_2017
```
Demo.py's parameter
```bash
    -m Path to model folder 
    -v video path, if None will use your webcam
    -vw video output file name
```    