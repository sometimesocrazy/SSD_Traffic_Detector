import json
from collections import namedtuple

import cv2
import matplotlib.pyplot as plt

from Modules.Entities.DetectedObject import DetectedObject

traffic_dict = {1: 'bien bao khac', 2: 'stop', 3: 're trai', 4: 're phai', 5: 'cam re trai', 6: 'cam re phai',
                7: 'duong 1 chieu', 8: 'speed limit', 9: 'minimum 30', 10: 'end minimum 30', 11: 'bien soc trang'}
lines = open(r'D:\Github\SSD_Traffic_Detector\Files\new_answer.txt', 'r').read().split('\n')
selected_id = 3
print(lines)
true_objects = {}
for line in lines:
    vars = line.split(' ')
    if len(vars) != 6:
        print('ERROR : {}'.format(vars))
        continue
    # true_objects[str]
    vars = [int(var) for var in vars]
    if int(vars[1]) == selected_id:
        true_objects[vars[0]] = DetectedObject(selected_id, 1.0, vars[2:])

predicted_frames = json.loads(open(r'D:\Github\SSD_Traffic_Detector\Files\out_data_frame_by_frame.json').read())
predicted_objects = {}
for frame in predicted_frames:
    # for obj in predicted_frames[frame]:
    objs = predicted_frames[frame]
    for obj in objs:
        if obj['label']['id'] == selected_id + 1:
            if frame not in predicted_objects:
                predicted_objects[frame] = [obj]
            else:
                predicted_objects[frame].append(obj)
union = 0
overlap = 0


def compute_area(box):
    dx = box.xmax - box.xmin
    dy = box.ymax - box.ymin
    return dx * dy


Rectangle = namedtuple('Rectangle', 'xmin ymin xmax ymax')


def area(a, b):  # returns None if rectangles don't intersect
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx >= 0) and (dy >= 0):
        return dx * dy
    else:
        return 0


cap = cv2.VideoCapture(r"D:\Github\SSD_Traffic_Detector\QuestionVideo.avi")
ious = []
for frame in true_objects:
    true_object = true_objects[frame]
    key = str(frame)
    if key in predicted_objects:
        predict_object = predicted_objects[key][0]
        predict_rect = Rectangle(predict_object['bbox'][1], predict_object['bbox'][0], predict_object['bbox'][3],
                                 predict_object['bbox'][2])

    else:
        predict_are = 0
        predict_rect = Rectangle(0, 0, 0, 0)

    true_rect = Rectangle(true_object.box[0], true_object.box[1], true_object.box[2], true_object.box[3])

    frame_union = compute_area(true_rect) + compute_area(predict_rect)
    frame_over = area(predict_rect, true_rect) * 2
    union += frame_union
    overlap += frame_over
    ious.append(frame_over / frame_union)
    # print('over : {}\t union : {}\nIoU : {}'.format(frame_over,frame_union,frame_over/frame_union))
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
    ret, mat = cap.read()
    cv2.rectangle(mat, (true_rect.xmin, true_rect.ymin), (true_rect.xmax, true_rect.ymax), (255, 255, 255), 2)
    cv2.rectangle(mat, (int(predict_rect.xmin), int(predict_rect.ymin)),
                  (int(predict_rect.xmax), int(predict_rect.ymax)), (0, 255, 0), 2)
    cv2.imshow('', mat)
    k = cv2.waitKey(1)
    # if k==ord('s'):
    #     print('pause')
    #     cv2.waitKey(0)
plt.plot(ious)
plt.show()
print(overlap / union)

# print(union)
