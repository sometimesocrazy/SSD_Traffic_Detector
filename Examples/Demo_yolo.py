import json
import os
import sys
import time

import cv2
import numpy as np
from PIL import Image

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from Modules.Module.Yolo_Detector import YOLO
from argparse import ArgumentParser


def build_args_parser():
    parser = ArgumentParser()
    parser.add_argument("-v", "--video", type=str, help="Path to video", required=False)
    parser.add_argument("-vw", "--videowrite", type=str, help="Path to video writeout", required=False)

    parser.add_argument("-m", "--model_path", type=str, help="Path to model folder", required=True)

    return parser


args = build_args_parser().parse_args()
video_path = args.video
video_out_patch = args.videowrite
if video_path is not None:
    cap = cv2.VideoCapture(video_path)
else:
    cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = None
# cv2.VideoWriter('output.avi', fourcc, 40.0, (size[0], size[1]))
detector = YOLO()
frameindex = -1
detectAbleFrame = 0
points = []
lines = []
frameBuffered = []
framebufferSize = 15


def process_predict_object(object):
    print(str(object.label['name']) + ' : ' + str(object.score))


def rotateImage(image, angle):
    """
    Rotate image
    :param image: image mat
    :param angle:  rotate value
    :return: image rotated mat
    """
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


out_data = {}

def smooth_frames(frames, selected_traffic=None):
    """
    detect image in multi continuous frames
    :param frames: continuous frames
    :param selected_traffic: id of selected traffic sign
    :return:
    """
    points = []
    trurth_objects = []
    predict_objects = []
    for frame in frames:
        frame_trurth_objects = []
        for object in frame.detected_objects:
            point = object.GetCentroid()
            if object.score >= .5 and (object.label['id'] == selected_traffic or selected_traffic is None):
                frame_trurth_objects.append(object)
                points.append([object.label['id'], point,object.score])
                trurth_objects.append(object)
        if len(frame_trurth_objects) == 0:
            for i in range(len(frame.detected_objects)):
                predict_object = frame.detected_objects[i]
                for point in points:
                    if predict_object.isInside(point[1]) and predict_object.BoxRatio() < 1.5 and predict_object.label['id'] == point[0] :
                        # object dự đoán được đặt ở đây

                        predict_objects.append(predict_object)
                        mat = predict_object.visualize(frame.image)
                        cv2.imshow('predict', mat)
                        cv2.waitKey(1)

    for object in predict_objects:
        points.append([object.label['name'], object.GetCentroid()])

    all_objects = predict_objects + trurth_objects
    score_label = {}
    for i in range(len(all_objects)):
        a = all_objects[i]

        if a.label['id'] in score_label:
            score_label[a.label['id']].append(a.score)
        else:
            score_label[a.label['id']] = [a.score]

    for i in range(len(all_objects)):
        a = all_objects[i]
        scores = score_label[a.label['id']]
        all_objects[i].score = sum(scores)/len(scores)


    for object in all_objects:
        out_data[object.frame] = [object.to_dict()]



i = 0
while True:
    start_time = time.time()
    ret, image_np = cap.read()
    frameindex += 1
    if image_np is None:
        break
    i += 1
    image = Image.fromarray(image_np)
    imagedetected = detector.detect_image(image,frameindex)

    detected_objects = imagedetected.detected_objects

    # out_data[frameindex] = [detected_object.to_dict() for detected_object in detected_objects]
    # image_np = imagedetected.visualize()
    # cv2.imshow('frame', image_np)
    # print(str(frameindex*100/maxframe)+" %")
    # print("FPS: ", round(1 / (time.time() - start_time)), end='\r')
    if len(frameBuffered) > framebufferSize:
        frameBuffered.pop(0)
        cv2.imshow('', frameBuffered[framebufferSize - 1].image)

    frameBuffered.append(imagedetected)

    smooth_frames(frameBuffered,2)  # sửa dòng này chọn loại biển

    # imagedetected = frameBuffered[len(frameBuffered) - 1]
    # detected_objects = imagedetected.detected_objects
    # if len(detected_objects) > 0 and detected_objects[0].score > .5:
    #     out_data[frameindex] = [
    #         detected_objects[0].to_dict()]  # [detected_object.to_dict() for detected_object in detected_objects]

    if video_out_patch is not None:
        if out is None:
            h, w, c = frameBuffered[len(frameBuffered) - 1].image.shape
            forcc = cv2.VideoWriter_fourcc(*'XVID')
            out = cv2.VideoWriter(frameBuffered[len(frameBuffered) - 1].image, forcc, 10.0, (w, h))
        else:
            out.write(frameBuffered[len(frameBuffered) - 1].image)
    k = cv2.waitKey(1)
    # if k == ord('q'):
    #     cv2.destroyAllWindows()
    #     break
    # elif k == ord('s'):
    #     print("Save image")
    #     cv2.imwrite(time.strftime("%Y_%m_%d %H_%M_%S") + '.png', image_np)

cap.release()
if out is not None:
    out.release()
cv2.destroyAllWindows()
open('F:\Github\SSD_Traffic_Detector\Files\out_data_yolo_multi_frames_2.json', 'w').write(json.dumps(out_data, indent=4))
print("EXIT !")
